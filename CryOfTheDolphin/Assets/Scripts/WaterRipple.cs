﻿using UnityEngine;
using System.Collections;

public class WaterRipple : MonoBehaviour {

    public float scrollSpeed;

    void Start()
    {
        scrollSpeed = 30f;
    }

    void Update()
    {
        //SpriteRenderer sr = GetComponent<SpriteRenderer>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();

        Material material = meshRenderer.material;

        //material.GetTextureOffset("_MainTex");
        Vector2 offset = material.mainTextureOffset;

        offset.y += Time.deltaTime / scrollSpeed;

        material.mainTextureOffset = offset;

    }
}
