﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using UnityEngine.Cloud.Analytics;

public enum GameState
{
    MENU,
    GAME
};

public delegate void OnStateChangeHandler();
public class GameManager : MonoBehaviour
{
    public event OnStateChangeHandler OnStateChange;
    public GameState gameState { get; private set; }

    private Game game;
    private int highScore;


       
    public static GameManager instance = null;
    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }

    void Start()
    {
        game = Game.gameInstance;
        Load();
        //Debug.Log(Application.persistentDataPath);
    }

	void Update()
	{

	}
    

    public void SetGameState(GameState state)
    {
        this.gameState = state;
        if (OnStateChange != null)
        {
            OnStateChange();
        }
    }

    

    public void Save(int score)
    {
        if (score > highScore)
        {
            if (Application.platform == RuntimePlatform.WindowsWebPlayer)
            {
                PlayerPrefs.SetInt("score", highScore);
            }
            else
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(Application.persistentDataPath + "/CryOfTheDolphin.dat");

                PlayerData data = new PlayerData();

                data.highScore = score;
                highScore = score;

                bf.Serialize(file, data);
                file.Close();
            }
        }

    }

    public void Reset()
    {
        if (Application.platform == RuntimePlatform.WindowsWebPlayer)
        {
            PlayerPrefs.DeleteKey("score");
        }
        else
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/CryOfTheDolphin.dat");

            PlayerData data = new PlayerData();

            data.highScore = 0;


            bf.Serialize(file, data);
            file.Close();
        }

    }

    public void Load()
    {
        if (Application.platform == RuntimePlatform.WindowsWebPlayer)
        {
            highScore = PlayerPrefs.GetInt("score");
        }
        else if (File.Exists(Application.persistentDataPath + "/CryOfTheDolphin.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/CryOfTheDolphin.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            highScore = data.highScore;
        }
        else
        {
            highScore = 0;
        }

    }

    public int getHighscore()
    {
        return highScore;
    }

    public void saveAnalytics()
    {
        /*StreamWriter file = File.AppendText(Application.persistentDataPath + "/CryOfTheDolphin_analytics.txt");
        file.WriteLine("=== Play: " + DateTime.Now.ToString("yyyy/MM/dd - HH:mm:ss") + "===");
        file.WriteLine("Time played: " + game.getTimePlayed());
        file.WriteLine("Distance travelled: " + game.distanceTravelled);
        file.WriteLine("Echoes fired: "+game.echoCount);
        file.WriteLine("Echoes hit: "+game.echoHit);
        file.WriteLine("Echoed fish: "+game.echoFish);
        file.WriteLine("Echoed shark: "+game.echoShark);
        file.WriteLine("");
        file.Close();*/

		UnityAnalytics.CustomEvent("gameOver", new Dictionary<string, object>
		{
			{ "timePlayed", game.getTimePlayed() },
			{ "echoFired", game.echoCount },
			{ "echoHit", game.echoHit },
			{ "echoHitFish", game.echoFish },
			{ "echoHitShark", game.echoShark },
			{ "distanceTravelled", game.distanceTravelled }
		});
    }

    [Serializable]
    class PlayerData
    {
        //serialized data
        public int highScore;

    }

    /*
    public int HighScore
    {
        set 
        {
            if (value > highScore)
            {
                highScore = value;
                Save();
            }
        }
    }*/

    public void OnApplicationQuit()
    {
        GameManager.instance = null;
    }
}
