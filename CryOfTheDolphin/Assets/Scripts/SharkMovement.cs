﻿using UnityEngine;
using System.Collections;

public class SharkMovement : MonoBehaviour
{

    public float speed;
    public float minRange;
    public int minWait, maxWait;

	private Game game;

    enum SHARKSTATE
    {
        IDLE,
        MOVE,
        CHASE,
		ECHO_CHASE,

    }


    SHARKSTATE curState;
    Transform playerTransform;
    Vector3 playerTarget, randomTarget, current, echoSourceTarget;
    int maxX, maxY, minX, minY;
    int ranTargetX, ranTargetY;
    float ranWaitTime;

	private EchoReactor echoReactor;


    


    // Use this for initialization
    void Start()
    {
		game = Game.gameInstance;
        // need to get reference of player's transform in order to follow
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        playerTarget = playerTransform.position;
		echoReactor = GetComponent<EchoReactor> ();
		echoSourceTarget = echoReactor.getSourcePosition();
        curState = SHARKSTATE.IDLE;
        current = transform.position;

        maxX = 683;
        maxY = 384;
        minX = -maxX;
        minY = -maxY;
        minWait = 2;
        maxWait = 5;
        ranWaitTime = Random.Range(minWait, maxWait);
        reCalculateTarget();
    }

    // Update is called once per frame
    void Update()
    {

        if (curState == SHARKSTATE.IDLE)
            ranWaitTime -= Time.deltaTime; 
        current = transform.position;
        playerTarget = playerTransform.position;
        playState();
        //check player echo
        //Debug.Log(Vector3.Distance(current, playerTarget));

    }

    void reCalculateTarget()
    {
        ranTargetX = Random.Range(minX, maxX);
        ranTargetY = Random.Range(minY, maxY);
        randomTarget = new Vector3(ranTargetX, ranTargetY, -2);
    }

    void moveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, randomTarget, speed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, randomTarget - transform.position);
    }

    bool moveComplete(Vector3 curPos, Vector3 destPos)
    {
        if (curPos == destPos)
            return true;
        return false;
    }

    bool checkPlayer()
    {
		if (!game.gameOver) 
		{
			if (Vector3.Distance (transform.position, playerTarget) <= minRange)
				return true;
		}
        return false;

    }

	bool checkEchoSource()
	{
        echoSourceTarget = echoReactor.getSourcePosition();
		if (echoSourceTarget != Vector3.zero/* && Vector3.Distance(transform.position, echoSourceTarget) <= minRange*/)
			return true;
		return false;
		
	}

    void playState()
    {
        switch (curState)
        {
            case SHARKSTATE.IDLE:

                if (ranWaitTime <= 0)
                {
                    reCalculateTarget();
                    ranWaitTime = 0;
                    curState = SHARKSTATE.MOVE;
                }

                break;

            case SHARKSTATE.MOVE:

                if (moveComplete(current, randomTarget))
                {
                    ranWaitTime = Random.Range(minWait, maxWait);
                    curState = SHARKSTATE.IDLE;
                }

                else if (checkPlayer())
                {
                    curState = SHARKSTATE.CHASE;
                }
                else
                {
                    moveToTarget();
                }


                break;
            case SHARKSTATE.CHASE:

                playerTarget = playerTransform.position;
                transform.rotation = Quaternion.LookRotation(Vector3.forward, playerTarget - transform.position);
                transform.position += transform.up * speed * Time.deltaTime;


                if (Vector3.Distance(transform.position, playerTarget) >= minRange)
                {
                    curState = SHARKSTATE.IDLE;
                }

                break;
            case SHARKSTATE.ECHO_CHASE:

                echoSourceTarget = echoReactor.getSourcePosition();
                transform.rotation = Quaternion.LookRotation(Vector3.forward, echoSourceTarget - transform.position);
                transform.position += transform.up * speed * Time.deltaTime;
                //Debug.Log(echoSourceTarget);


                if (echoReactor.getSourcePosition() == Vector3.zero || Vector3.Distance(transform.position, echoSourceTarget) <= 3.0f)
                {
                    echoReactor.setSourcePosition(Vector3.zero);
                    curState = SHARKSTATE.IDLE;
                }

                break;
            default:
                break;
        }
        if (checkEchoSource() && curState != SHARKSTATE.CHASE)
        {
            curState = SHARKSTATE.ECHO_CHASE;
        }
		else if (game.GameOver) {
			curState = SHARKSTATE.MOVE;
		}
    }


}
