﻿using UnityEngine;
using System.Collections;

public class PauseLabel : MonoBehaviour {

	private Game game;
	private SpriteRenderer spriteRenderer;

	void Start () {
		game = Game.gameInstance;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		spriteRenderer.enabled = false;
	}

	void Update () {
		if (game.pause) {
			spriteRenderer.enabled = true;
		} else {
			spriteRenderer.enabled = false;
		}
	}
}
