﻿using UnityEngine;
using System.Collections;

public class SeaDarkness : MonoBehaviour
{
    public bool updateDepth { get; set; }

    private float decrementAmount = 0.2f;
    private float speed = 1.5f;
    private float duration = 10f;

    MeshRenderer meshRenderer;
    Material material;



    // Use this for initialization
    void Start()
    {
        updateDepth = false;
    }

    void Update()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        material = meshRenderer.material;
        checkDepth();
    }

    void checkDepth()
    {
        if(updateDepth)
        {
            Vector4 matColor = material.color;
            matColor.x -= decrementAmount;
            matColor.y -= decrementAmount;
            matColor.z -= decrementAmount;
            material.color = matColor;
            updateDepth = false;
        }
    }

}
