﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {

    private Game game;
    private GameManager gameManager;
	public AudioSource fishEaten, death, goldenBoys;
	public GameObject scoreParticle;

	// Use this for initialization
	void Start () {
        game = Game.gameInstance;
        gameManager = GameManager.instance;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("EchoFood"))
        {
            Destroy(coll.gameObject);
            //fishEaten.Play();
			goldenBoys.Play();
            Instantiate(scoreParticle, transform.position, Quaternion.identity);
            GetComponent<PlayerEcho>().unlockEcho();
        }
        if(coll.gameObject.CompareTag("Food"))
        {
            //Fish code
            //game.incrementScore();
            game.activatePause();
            Destroy(coll.gameObject);
			fishEaten.Play();
			Instantiate(scoreParticle,transform.position,Quaternion.identity);
            //Debug.Log("Fish Touched, score added");
        }
        if(coll.gameObject.CompareTag("Enemy"))
        {
            if (!game.GameOver)
            {
                gameManager.Save(game.playerScore);
                game.GameOver = true;
                death.Play();
                GetComponent<PlayerEcho>().disableEchoBar();
                game.saveAnalytics();
            }
            //Shark code
        }
    }
}
