﻿using UnityEngine;
using System.Collections;

public class InstructionsMenu : MonoBehaviour {

	public Texture2D nextButton, previousButton, startButton;
	public Texture2D[] instructions = new Texture2D[4];

	private Texture2D current;
	private int arrayPosition;
	private int width = Screen.width;
	private int height = Screen.height;
	private float groupPosX, groupPosY, groupWidth, groupHeight,
				  	texPosX, texPosY, texWidth, texHeight;
				  			

	void Start () {
		arrayPosition = 0;
		current = instructions [arrayPosition];

		texPosX = width / 2f - width / 9.401f;
		texPosY = height / 2f - height / 2.667f;
		texWidth = width / 4.7f; //272.3px
		texHeight = height / 2.13333333333f;

		groupPosX = width / 2f - width / 8.533f;
		groupPosY = height / 2f - height / 7.273f;
		groupWidth = width / 4.26615f;
		groupHeight = height / 2.666f;
	}
	
	void Update () {
		current = instructions [arrayPosition];
	}

	void OnGUI() {

		GUI.DrawTexture (new Rect (texPosX, texPosY, texWidth, texHeight), current);

		if (arrayPosition < 3) {
			GUI.BeginGroup (new Rect (groupPosX, groupPosY, groupWidth, groupHeight));
			if (GUI.Button (new Rect (groupWidth * 0, groupHeight / 1.4f, 
								    groupWidth / 2.4f, groupHeight / 6f), previousButton)) {
				Previous ();
			}
			if (GUI.Button (new Rect (groupWidth / 1.714f, groupHeight / 1.4f, 
				    				groupWidth / 2.4f, groupHeight / 6f), nextButton)) {
				Next ();
			}
			GUI.EndGroup ();
		}

		else { //(arrayPosition == 3) {
			GUI.BeginGroup (new Rect (groupPosX, groupPosY, groupWidth, groupHeight));
			if (GUI.Button (new Rect (groupWidth * 0, groupHeight / 1.4f, 
				groupWidth / 2.4f, groupHeight / 6f), previousButton)) {
				Previous ();
			}
			if (GUI.Button (new Rect (groupWidth / 1.714f, groupHeight / 1.4f, 
				groupWidth / 2.4f, groupHeight / 6f), startButton)) {
				StartGame ();
			}
			GUI.EndGroup ();
		}
	}

	void Previous() {
		if (arrayPosition != 0) {
			arrayPosition -= 1;
		}
	}

	void Next() {
		if (arrayPosition != 3) {
			    arrayPosition += 1;
			}
	}
	
	void StartGame() {
		Application.LoadLevel(1);
	}
	
	void ReturnToMain() {
		Application.LoadLevel(0);
	}

}
