﻿using UnityEngine;
using System.Collections;

public class PauseManager : MonoBehaviour
{

    public Game game;
    public AudioSource pause, unpause;
    public GameObject resumeSpr, exitSpr, musicLabel, sfxLabel;
    private SpriteRenderer resumeSR, exitSR;

    private const float SCALEAMT = 1.5f;
    private bool paused = false;
    GameManager GM;
    Ray ray;

    void Awake()
    {
        GM = GameManager.instance;
    }

    void Start()
    {
        game = Game.gameInstance;
        resumeSR = resumeSpr.GetComponent<SpriteRenderer>();
        exitSR = exitSpr.GetComponent<SpriteRenderer>();

        resumeSR.enabled = false;
        exitSR.enabled = false;
        musicLabel.SetActive(false);
        sfxLabel.SetActive(false);

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

    }

    void ExitGame()
    {
        game.togglePause();
        Application.LoadLevel(0);
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !game.gameOver)
        {
            game.pause = !game.pause;
            toggleButtons();
        }

        if (game.pause)
        {
            slowMusic();
        }
        else
        {
            game.backgroundLoop.pitch = 1;
        }

    }

    
    public void toggleButtons()
    {
        resumeSR.enabled = !resumeSR.enabled;
        exitSR.enabled = !exitSR.enabled;
        musicLabel.SetActive(exitSR.enabled);
        sfxLabel.SetActive(exitSR.enabled);
    }

    void slowMusic()
    {
        game.backgroundLoop.pitch = 0.75f;
    }

}