﻿using UnityEngine;
using System.Collections;

public class FishMovement : MonoBehaviour
{

    public float speed;
    public int minWait, maxWait;

    enum FISHSTATE
    {
        IDLE,
        MOVE

    };

    FISHSTATE curState;
    int maxX, maxY, minX, minY;
    int ranTargetX, ranTargetY;
    float ranWaitTime;
    Vector3 randomTarget, current;

    // Use this for initialization
    void Start()
    {

        curState = FISHSTATE.IDLE;
        maxX = 683;
        maxY = 384;
        minX = -maxX;
        minY = -maxY;
        minWait = 2;
        maxWait = 5;
        ranWaitTime = Random.Range(minWait, maxWait);
        current = transform.position;
        reCalculateTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (curState == FISHSTATE.IDLE)
            ranWaitTime -= Time.deltaTime; 
        current = transform.position;
        checkInput();
        playState();
    }

    void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            curState = FISHSTATE.MOVE;
        }
    }

    void reCalculateTarget()
    {
        ranTargetX = Random.Range(minX, maxX);
        ranTargetY = Random.Range(minY, maxY);
        randomTarget = new Vector3(ranTargetX, ranTargetY, -2);
    }

    void moveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, randomTarget, speed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(Vector3.forward, randomTarget - transform.position);
    }

    bool moveComplete(Vector3 curPos, Vector3 destPos)
    {
        if (curPos == destPos)
            return true;
        return false;
    }


    void playState()
    {
        switch (curState)
        {
            case FISHSTATE.IDLE:

                
                if (ranWaitTime <= 0)
                {
                    reCalculateTarget();
                    ranWaitTime = 0;
                    curState = FISHSTATE.MOVE;                    
                }
                break;
            case FISHSTATE.MOVE:

                moveToTarget();
                if (moveComplete(current, randomTarget))
                {
                    ranWaitTime = Random.Range(minWait, maxWait);
                    curState = FISHSTATE.IDLE;
                }

                
                break;
            default:
                break;
        }

    }
}


