﻿using UnityEngine;
using System.Collections;

public class Echo : MonoBehaviour {

    private Game game;
    public float range;
    public float speed;
	public float widthScaleMax = 3;
    private Vector3 direction;
    private Vector3 spawnLocation;
    private float distance;
    private float scale;
    private float colliderScale;
	private Vector3 sourcePosition;
	public AudioSource[] echoPings = new AudioSource[2];

	// Use this for initialization
	void Start () {
        scale = 0.0f;
        spawnLocation = transform.position;
        colliderScale = 1;
		echoPings [Random.Range (0, 2)].Play ();
        game = Game.gameInstance;
        game.echoCount++;
	}
	
	// Update is called once per frame
	void Update () 
    {
        updateMovement();
        updateDistance();
	}

    private void updateDistance()
    {
        distance = Vector3.Distance(spawnLocation, transform.position);
        if (distance >= range)
        {
            Destroy(gameObject);
        }
    }

    private void updateMovement()
    {
        transform.position += direction * Time.deltaTime * speed;
        transform.position = new Vector3(transform.position.x, transform.position.y, -2);
        scale += 15.0f * Time.deltaTime;
        colliderScale += 90.0f * Time.deltaTime;
		transform.localScale = new Vector3(Mathf.Clamp(1.00f + scale, 0, widthScaleMax), 1.00f + scale, 0);
        //gameObject.GetComponent<BoxCollider2D>().size = new Vector2(colliderScale, colliderScale);
		gameObject.GetComponent<BoxCollider2D>().size = new Vector2(1.00f + scale, 45.00f + scale);
        //gameObject.GetComponent<Rigidbody2D>().transform.localScale = transform.localScale;
    }

    public void setDirection(Vector3 direction)
    {
        this.direction = direction / direction.magnitude;

        float angle = Mathf.Atan2(this.direction.y, this.direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

	public void setSourcePosition(Vector3 position)
	{
		sourcePosition = position;
	}

	public Vector3 getSourcePosition()
	{
		return sourcePosition;
	}
}
