﻿using UnityEngine;
using System.Collections;

public class BubbleSound : MonoBehaviour {

	public AudioSource[] bubble = new AudioSource[3];
	private int play;

	void Start () {
	}

	void Awake () {
		play = (int)Random.Range (0, 100);
		if (play <= 27) {
			int r = (int)Random.Range (0, 3);
			bubble [r].pitch = 0.5f + 0.2f * Random.value;
			bubble [r].Play ();
		}
	}

}
