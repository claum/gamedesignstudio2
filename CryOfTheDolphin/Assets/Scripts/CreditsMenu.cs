﻿using UnityEngine;
using System.Collections;

public class CreditsMenu : MonoBehaviour {

	void OnGUI() {
		GUI.BeginGroup (new Rect (Screen.width / 2 - 175, Screen.height / 2 + 220, 500, 500));
		GUI.Box (new Rect (0, 0, 350, 80), " ");
		if (GUI.Button (new Rect (50, 20, 100, 40), "Start Game")) {
			StartGame ();
		}
		if (GUI.Button (new Rect (200, 20, 100, 40), "Main Menu")) {
			ReturnToMain ();
		}
		GUI.EndGroup ();
	}

	void StartGame() {
		Application.LoadLevel(1);
	}

	void ReturnToMain() {
		Application.LoadLevel(0);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
