﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    Game game;
    //sine wave
    public float frequency;
    public float magnitude;
    Vector3 axis;
    Vector3 sineWave;

    private bool paused = false;
    public float speed = 1.5f;
    float angle;
    Vector3 target;//, current;
	Quaternion lastRotation;

	//public AudioSource swim;

    void Start()
    {
        axis = Vector3.right;

        target = transform.position;
        //current = transform.position;
        game = Game.gameInstance;
    }

    void Update()
    {
        if (!paused && !game.gameOver)
        {
            checkInput();
            

			if(Vector3.Distance(transform.position,target) > 10)
			{
                sineWave = axis * Mathf.Sin(Time.time * frequency) * magnitude;
                transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime) + sineWave;
				transform.rotation = Quaternion.LookRotation(Vector3.forward, target - transform.position);
				//Debug.Log (Vector3.Normalize(transform.rotation.eulerAngles));
				//Debug.Log (Quaternion.Euler(0, -45, 0) * Vector3.right);
				axis = Quaternion.Euler(transform.rotation.eulerAngles) * Vector3.right;
			}

            
        }
		//if (transform.position.magnitude >= 0.1f)
		//	swim.Play ();
    }

    void checkInput()
    {
        if (Input.GetKey (KeyCode.W)) 
		{
			target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			target.z = transform.position.z;
			game.distanceTravelled += (speed * Time.deltaTime);
		} 

    }

    public bool Paused
    {
        set { paused = value; }
    }
}