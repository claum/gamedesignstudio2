﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour
{

    #region Fields
    public int type;

    // accelerate, decrease size, remove
    private float speed;
    private float size;
    private float decreaseAmount;
    private float incremenatAmount;
    private float frequency;  // Speed of sine movement
    private float magnitude;   // Size of sine movement
    private float index;

    private Vector3 axis;
    private Vector3 pos;

    private float speedMin = 40f;
    private float speedMax = 200f;
    private float sizeMin = 0.5f;
    private float sizeMax = 2f;
    private float decreaseAmountMin = 0.2f;
    private float decreaseAmountMax = 0.5f;
    private float incrementMin = 10f;
    private float incrementMax = 50f;
    private float freqMin = 5f;
    private float freqMax = 10f;
    private float magMin = 2f;
    private float magMax = 10f;
    #endregion

    // Use this for initialization
	void Start () {
        if (type == 0)
        {
            speed = Random.Range(speedMin, speedMax);
        }else if(type == 1)
        {
            speed = Random.Range(speedMin*2, speedMax * 2);
        }
        size = Random.Range(sizeMin, sizeMax);
        decreaseAmount = Random.Range(decreaseAmountMin, decreaseAmountMax);
        incremenatAmount = Random.Range(incrementMin, incrementMax);
        frequency = Random.Range(freqMin, freqMax);
        magnitude = Random.Range(magMin, magMax);

        transform.position = new Vector3(Random.Range(Constants.XMIN, Constants.XMAX), transform.position.y, transform.position.z);

        pos = transform.position;
        axis = transform.right;  

        transform.localScale = new Vector3(size, size, 0);

	}
	
	// update is called once per frame
	void Update () {
        
        updateMove();
        updateScale();

	}
    void updateScale()
    {
        Vector3 scaleAmount;

        if (transform.localScale.x > 0)
        {
            size = decreaseAmount;
            scaleAmount = new Vector3(size, size, 0);
            transform.localScale -= scaleAmount * Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void updateMove()
    {
        if(speed <= speedMax)
            speed += incremenatAmount * Time.deltaTime;

        pos += transform.up * Time.deltaTime * speed;
        transform.position = pos + axis * Mathf.Sin(Time.time * frequency) * magnitude;
    }
}
