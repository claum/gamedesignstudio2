﻿using UnityEngine;
using System.Collections;

public class EchoParticle : MonoBehaviour {

    public float deathTimer = 2;
    public float scaleAdd = 20000;
    private float deathTimerTick;
    private float scale;
	private SpriteRenderer spriteRenderer;

	public AudioSource echoResponse;
	// Use this for initialization
	void Start () 
    {
        scale = 0.0f;
		echoResponse.pitch = 0.7f + 0.3f * Random.value;
		echoResponse.Play ();
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () 
    {
        updateMovement();
        tickDeathTimer();
	}

    private void updateMovement()
    {
        scale += scaleAdd * Time.deltaTime;
        scaleAdd /= 1.01f;
        transform.localScale = new Vector3(1+scale, 1+scale, 0);
    }

    private void tickDeathTimer()
    {
        deathTimerTick += Time.deltaTime;

		//if (deathTimerTick > (deathTimer / 1.5)) 
		//{
			Color c = spriteRenderer.color;
			//c.a = (float)(1 - (deathTimerTick / (deathTimer - (deathTimer / 1.5))));
			c.a = (float)(1 - (deathTimerTick / deathTimer));
			spriteRenderer.color = c;
		//}

        if(deathTimerTick>=deathTimer)
        {
            Destroy(gameObject);
        }
    }
}
