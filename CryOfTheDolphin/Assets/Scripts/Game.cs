﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Game : MonoBehaviour
{

    private GameManager GM;
    [SerializeField]
    Camera mainCam;

    #region Fields
    public Text scoreText;
    public Text fishText;
    public Text gameOverText;
    public GameObject playerPrefab;
    public GameObject seaWeedPrefab;
    public GameObject fishPrefab;
    public GameObject echoFishPrefab;
    public GameObject sharkPrefab;
    public GameObject sea;
    public GameObject instructionsMove;
    public GameObject instructionsEcho;
    private GameObject playerInstance;
    private PlayerMovement playerMovement;
    SeaDarkness seaScript;

    public static Game gameInstance = null;

    public int playerScore { get; private set; }
    private int fishLeft;
    public bool gameOver;
    public float shakeAmount;
    private bool shakeNow;
    private float shakeTime;		// For camera shakes
    private Vector3 positionToGo;	// For camera shakes
    private float pauseEffect;
    private int seaWeedAmt = 15;
     
    public bool pause { get; set; }
    public int level { get; set; }
    public float playerVision { get; set; }
    public int echoCount { get; set; }
    public int echoHit { get; set; }
    public int echoShark { get; set; }
    public int echoFish { get; set; }
	public int sharkCount;
    public float secondsPlayed { get; set; }
    public float distanceTravelled { get; set; }
    #endregion

    public AudioSource scoreConfirm, backgroundLoop, deathLoop, pauseSound, unpauseSound;


    void Awake()
    {
        gameInstance = this;
        GM = GameManager.instance;
        GM.OnStateChange += HandleOnStateChange;
        mainCam = GetComponent<Camera>();

        seaScript = sea.GetComponent<SeaDarkness>();
    }
    public void HandleOnStateChange()
    {
        Debug.Log("OnStateChange!");
    }
    void Start()
    {
        playerScore = 0;
        fishLeft = 0;
        level = 0;
        playerVision = Constants.startVision;
        gameOver = false;
        pause = false;
        shakeNow = false;
        pauseEffect = 0.01f;
        resetAnalytics();

        backgroundLoop.loop = true;
        backgroundLoop.Play();

        deathLoop.loop = true;
        deathLoop.mute = true;
        deathLoop.Play();

        instructionsMove.SetActive(false);
        instructionsEcho.SetActive(false);

        spawnPlayer();
        spawnSeedWeed();
        

    }

    void Update()
    {
        if (!pause)
        {
            Time.timeScale = 1;
            playerMovement.Paused = false;
            backgroundLoop.pitch = 1;

            if (!gameOver)
            {
                //game State
                if (fishLeft == 0)
                {
                    level++;
                    updateLevel(level);
                }
                secondsPlayed += Time.deltaTime;
                updateScoreText();

                if (shakeNow && playerVision < Constants.minWarnDist)
                    cameraShake(0.2f);
            }
            else
            {
                //gameOver state
                gameOverDisplay();
                backgroundLoop.Stop();
                deathLoop.mute = false;
            }

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                unpauseSound.Play();
            }


        }
        else
        {
            Time.timeScale = 0;
            slowMusic();
            playerMovement.Paused = true;

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                pauseSound.Play();
            }
        }



    }

    void updateScoreText()
    {
        scoreText.text = "x " + playerScore;
        //fishText.text = "Fish Left: " + fishLeft;
    }

    void updateLevel(int level)
    {
        if (level == 3)
        {
            spawnObject(echoFishPrefab);
            fishLeft++;
        }
        else
        {
            for (int i = 0; i < level; i++)
            {
                spawnObject(fishPrefab);
                fishLeft++;
            }
        }

        if ((level % Constants.difficulty) == 0)
        {
            spawnObject(sharkPrefab);
			sharkCount++;
            seaScript.updateDepth = true;
        }
        if ((level % Constants.echoUpgrade) == 0)
        {
            spawnObject(echoFishPrefab);
            fishLeft++;
        }

        if (playerVision > Constants.minVision)
            playerVision -= Constants.visionLoss;
    }

    void spawnPlayer()
    {
        playerInstance = (GameObject)Instantiate(playerPrefab, new Vector3(0, 0, -2), Quaternion.identity);
        playerMovement = playerInstance.GetComponent<PlayerMovement>();
    }

    void spawnSeedWeed()
    {
        float randX, randY;
        Quaternion randRot;
        for (int i = 0; i < seaWeedAmt; i++)
        {
            randX = Random.Range(Constants.XMIN, Constants.XMAX);
            randY = Random.Range(Constants.YMIN, Constants.YMAX);
            randRot = Quaternion.Euler(0, 0, randX);

            Instantiate(seaWeedPrefab, new Vector3(randX, randY, 0), Quaternion.identity);
        }
    }

    void spawnObject(GameObject spawnObject)
    {
        float randX, randY;
        Quaternion randRot;

        //daum self comment: might need to rewrite this, doesn't seem efficient.
        randX = Random.Range(Constants.XMIN, Constants.XMAX);
        randY = Random.Range(Constants.YMIN, Constants.YMAX);
        randRot = Quaternion.Euler(0, 0, randX);

        if (!spawnPosIsLegal(new Vector2(randX, randY), Constants.safeSpawnRadius))
        {
            randX = Random.Range(Constants.XMIN, Constants.XMAX);
            randY = Random.Range(Constants.YMIN, Constants.YMAX);
        }

        Instantiate(spawnObject, new Vector3(randX, randY, -2), randRot);


    }

    bool spawnPosIsLegal(Vector2 pos, float radius)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(pos, radius);

        foreach (Collider2D collider in colliders)
        {
            GameObject go = collider.gameObject;

            if (go.transform.CompareTag("Player"))
            {
                return false;
            }
        }

        return true;
    }


    private void resetAnalytics()
    {
        echoCount = 0;
        echoHit = 0;
        secondsPlayed = 0;
        distanceTravelled = 0;
        echoShark = 0;
        echoFish = 0;
    }

    public void saveAnalytics()
    {
        GM.saveAnalytics();
        resetAnalytics();
    }

    void gameOverDisplay()
    {
        //strange number. Alpha doesn't seem to work well with GUI text. 
        //this should be later changed with gui image or something. Temp method;
        Color color = gameOverText.color;
        color.a += 0.02f;
        gameOverText.color = color;

		playerInstance.renderer.enabled = false;
    }

    void RestartGame()
    {
        Application.LoadLevel(1);
    }

    void MainMenu()
    {
        Application.LoadLevel(0);
    }

    void cameraShake(float amt)
    {
        shakeTime -= Time.deltaTime;

        positionToGo.x += Random.Range(-amt, amt);
        positionToGo.y += Random.Range(-amt, amt);
        positionToGo.z = mainCam.transform.position.z;

        mainCam.transform.position = positionToGo;

        if (shakeTime <= 0f)
        {
            shakeTime = 0.3f;
            shakeNow = false;
            mainCam.transform.position = new Vector3(0f, 0f, -10f);
        }
    }

    private IEnumerator Pause(float p)
    {
        if (Time.timeScale == 1f)
        {
            Time.timeScale = 0.1f;
            playerMovement.Paused = true;
        }
        yield return new WaitForSeconds(p);
        Time.timeScale = 1;
        playerMovement.Paused = false;
        
    }

    public void activatePause()
    {
        StartCoroutine(Pause(pauseEffect));
    }
    public void incrementScore()
    {
        playerScore += 1;
        fishLeft -= 1;
        scoreConfirm.Play();
    }

    public bool ShakeNow
    {
        set { shakeNow = value; }
    }

    public bool GameOver
    {
        set { gameOver = true; }
        get { return gameOver; }
    }

    public string getTimePlayed()
    {
        int minutes = 0;
        float seconds = secondsPlayed;
        while (seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }

        return minutes + "m " + (int)seconds + "s";
    }

    public bool togglePause()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            return false;
        }
        else
        {
            Time.timeScale = 0;
            return true;
        }
    }

    void slowMusic()
    {
        backgroundLoop.pitch = 0.75f;
    }

    public GameObject GetPlayer()
    {
        return playerInstance;
    }
}
