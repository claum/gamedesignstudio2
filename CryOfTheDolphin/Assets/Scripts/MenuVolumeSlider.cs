﻿using UnityEngine;
using System.Collections;

public class MenuVolumeSlider : MonoBehaviour {

	float volumeSlider;

	// Use this for initialization
	void Start () {
		volumeSlider = AudioListener.volume;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnGUI()
	{
		//menu layout
		//GUI.BeginGroup(new Rect(Screen.width / 2 - 187, Screen.height / 2 - 100, 600, 800));
		//GUI.Box(new Rect(0, 0, 400, 395), "\nMAIN MENU");

		GUI.Label (new Rect ((Screen.width / 2) - 20, Screen.height - 70, 100, 100), "");
		volumeSlider = GUI.HorizontalSlider(new Rect((Screen.width / 2) - 75, Screen.height - Screen.height/16, 150, 20),volumeSlider,0.0f,1.0f);
		AudioListener.volume = volumeSlider;
		//GUI.EndGroup();
	}
}
