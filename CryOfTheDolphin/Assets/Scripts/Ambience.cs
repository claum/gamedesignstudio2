﻿using UnityEngine;
using System.Collections;

public class Ambience : MonoBehaviour {

	public AudioSource ambience;
	//public AudioClip bend;
	//AudioSource audio;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("AmbientSound", Random.Range (3.0f, 10.0f), Random.Range (32.0f, 43.0f));
	}
	

	void AmbientSound() {
		ambience.pitch = 0.8f + 0.2f*Random.value;
		ambience.Play ();
	}


	// Update is called once per frame
	void Update () {
		//StartCoroutine(AmbientSound());
	}
}
