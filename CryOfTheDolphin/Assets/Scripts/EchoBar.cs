﻿using UnityEngine;
using System.Collections;

public class EchoBar : MonoBehaviour {

	private float halfWidth = 84;
	private float halfHeight = 10;
	private float percent = 1;
    private bool isOver;

	private static Texture2D _staticRectTexture;
	private static GUIStyle _staticRectStyle;
    Color blue;

	// Use this for initialization
	void Start () {
        isOver = false;
        blue = new Color(0, 0.3f, 0.8f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateBar(float curEcho, float maxEcho, bool isOver)
	{
		percent = (curEcho / maxEcho);
        this.isOver = isOver;
	}

	public void OnGUI()
	{
		GUI.Label (new Rect (10, 10, 100, 100), "");
		//GUIDrawRect (new Rect ((float)((Screen.width / 2) - halfWidth+2), (float)((Screen.height / 1.10) - halfHeight), ((halfWidth * 2)+1)*percent, halfHeight * 2),Color.white);
        Color c = blue;
        if (isOver)
        {
            c = Color.white;
        }

		GUIDrawRect(new Rect((float)((Screen.width/2)-(Screen.width/14.285714f)), ((Screen.height/1.10f)-halfHeight), (Screen.width/7.143f) * percent, (Screen.height/40f)), c);
	}

	public void GUIDrawRect( Rect position, Color color )
	{
		if( _staticRectTexture == null )
		{
			_staticRectTexture = new Texture2D( 1, 1 );
		}
		
		if( _staticRectStyle == null )
		{
			_staticRectStyle = new GUIStyle();
		}
		
		_staticRectTexture.SetPixel( 0, 0, color );
		_staticRectTexture.Apply();
		
		_staticRectStyle.normal.background = _staticRectTexture;
		
		GUI.Box( position, GUIContent.none, _staticRectStyle );
		
		
	}
}
