﻿using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour
{

    GameObject pulse;
    MeshRenderer pulseRenderer;
    //float maxPulseAmount, minPulseAmount;
    bool pulseActive;
    Color color;

    // Use this for initialization
    void Start()
    {

        pulse = GameObject.FindWithTag("Pulse");
        pulseRenderer = pulse.GetComponent<MeshRenderer>();
        color = pulseRenderer.material.color;
        //maxPulseAmount = 1f;
        //minPulseAmount = 0f;
        pulseActive = false;

    }

    void Update()
    {
        if(pulseActive)
        {
            activatePulse();
        }else
        {
            deactivatePulse();

        }
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.CompareTag("Player"))
        {
            pulseActive = true;
            Debug.Log("here");
        }
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            pulseActive = false;
        }
    }

    void activatePulse()
    {
        float amount =0;
        amount = Mathf.Abs(Mathf.Sin(Time.time)/2);
        
        color.a = amount;
        pulseRenderer.material.color = color;


    }
    void deactivatePulse()
    {
        color.a = Mathf.Lerp(pulseRenderer.material.color.a, 0, Time.deltaTime);
        pulseRenderer.material.color = color;

    }
}
