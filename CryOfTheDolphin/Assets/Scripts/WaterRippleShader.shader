﻿Shader "Custom/WaterRippleShader" {
	Properties {
		_MainTex ("Color (RGB) Alpha(A)", 2D) = "white" {}
		
		_Color("Color", Color) = (1,1,1,1)
		_Scale("Scale", float) = 1
		_Speed("Speed", float) = 1
		_Frequency("Frequency", float) = 1
		
	}
	SubShader {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert alpha vertex:vert

		sampler2D _MainTex;
		float _Scale, _Speed, _Frequency;
		half4 _Color;
		float _WaveAmplitude1;
		float _OffsetX1, _OffsetZ1;
		float _Distance1;
		float _xImpact1, _zImpact1, _xImpact2, _zImpact2,_xImpact3, _zImpact3,_xImpact4, _zImpact4;

		struct Input {
			float2 uv_MainTex;
		};
		
		void vert(inout appdata_full v)
		{
		half offsetvert = ((v.vertex.x * v.vertex.x) + (v.vertex. z * v.vertex.z));

		half value1 = _Scale * sin(_Time.w * _Speed + offsetvert * _Frequency);
		
		v.vertex.y += value1;
		v.normal.y += value1;
		
		}

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = tex2D (_MainTex, IN.uv_MainTex).a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
