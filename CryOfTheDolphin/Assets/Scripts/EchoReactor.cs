﻿using UnityEngine;
using System.Collections;

public class EchoReactor : MonoBehaviour {

    private Game game;
    public GameObject echoParticle;
    public float pingDelay = 0.5f;
    public bool isShark = false;

    private float pingTick;
    private bool canPing;
	private Vector3 sourcePosition;

	private float sourceTick;
	private float sourceDelay;

	public GameObject echoParticleWarning;
	public bool hasWarning;

	// Use this for initialization
	void Start () 
    {
        pingTick = pingDelay;
        canPing = true;
		sourceDelay = 180;
		sourceTick = sourceDelay;
        game = Game.gameInstance;

		if (hasWarning) 
		{
			Instantiate(echoParticleWarning,transform.position,Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () 
    {
        updatePing();
	}

	private void updateSource()
	{
		if (sourceTick >= sourceDelay) 
		{
			sourcePosition = Vector3.zero;
		} 
		else 
		{
			sourceTick += Time.deltaTime;
		}
	}

    private void updatePing()
    {
        if(pingTick>=pingDelay)
        {
            pingTick = pingDelay;
            canPing = true;
        }
        else 
        {
            pingTick+=Time.deltaTime;
            canPing = false;
        }
    }

    private void createPing()
    {
        if (canPing)
        {
            canPing = false;
            pingTick = 0;
            Instantiate(echoParticle,transform.position,Quaternion.identity);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        //Debug.Log("h");
        if (coll.gameObject.GetComponent<Echo>() != null)
        {
            createPing();
			sourcePosition = coll.gameObject.GetComponent<Echo>().getSourcePosition();
			sourceTick = 0;
            game.echoHit++;
            if(isShark)
            {
                game.echoShark++;
            }
            else 
            {
                game.echoFish++;
            }
        }
    }

	public Vector3 getSourcePosition()
	{
		return sourcePosition;
	}

    public void setSourcePosition(Vector3 pos)
    {
        sourcePosition = pos;
    }
}
