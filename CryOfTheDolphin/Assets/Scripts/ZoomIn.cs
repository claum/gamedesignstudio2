﻿using UnityEngine;
using System.Collections;

public class ZoomIn : MonoBehaviour {

    Game game;
    GameObject player;
    Camera mainCam;
    public float speed = 1.5f;
    private float minDist;
    private float zoomInAmount;
    private Vector3 initialCamPos;
    private float initialSize;

	// Use this for initialization
	void Start () {
        game = Game.gameInstance;
        //player = game.GetPlayerInstance();
        mainCam = Camera.main.GetComponent<Camera>();
        initialCamPos = mainCam.transform.position;
        initialSize = mainCam.orthographicSize;
        minDist = 200f;
        zoomInAmount = 10f;
        speed = 10f;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        //checkDistance();
	}

    void checkDistance()
    {
        float distance;
        distance = Vector3.Distance(transform.position, player.transform.position);
        Debug.Log(distance);

        if(distance < minDist)
        {
            Vector3 playerPos = new Vector3(player.transform.position.x, player.transform.position.y, mainCam.transform.position.z);
            Vector3 newPos = Vector3.MoveTowards(initialCamPos, playerPos, speed * Time.deltaTime);

            mainCam.transform.position = playerPos;
            mainCam.orthographicSize -= zoomInAmount * Time.deltaTime;
        }
        else
        {
            mainCam.transform.position = initialCamPos;
        }

    }
}
