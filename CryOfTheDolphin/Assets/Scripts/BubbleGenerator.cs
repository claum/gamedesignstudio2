﻿using UnityEngine;
using System.Collections;

public class BubbleGenerator : MonoBehaviour
{

    public GameObject bubble, bubble2;
    private Vector3 spawnValues;
    private int bubbleCount = 15;
    private float spawnWait = 0f;
    private float startWait = 0.3f;
    private float waveWait = 1f;
    private float randPos;

    void Start()
    {
        StartCoroutine(SpawnBubbles());
    }

    public void NextScene()
    {
        NextSceneBubbles();
    }


    void NextSceneBubbles()
    {
        for (int i = 0; i < 250; i++)
        {
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(bubble2, transform.position, spawnRotation);
        }
    }

    IEnumerator SpawnBubbles()
    {
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            for (int i = 0; i < bubbleCount; i++)
            {
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(bubble, transform.position, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}
