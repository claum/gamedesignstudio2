﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

    private Game game;

    private Transform playerTransform;
    private Vector3 playerPosition, currentPosition;
    private SpriteRenderer sprite;
    private Color color;

    private float minDist, dist;
    private float alphaAmount;

	// Use this for initialization
	void Start () {

        game = Game.gameInstance;

        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        sprite = GetComponent<SpriteRenderer>();
        color = sprite.color;
        playerPosition = playerTransform.position;
        currentPosition = transform.position;
        minDist = 0.0f;

	}
	
	// Update is called once per frame
	void Update () {

        minDist = game.playerVision;

        playerPosition = playerTransform.position;
        currentPosition = transform.position;

        dist = Vector3.Distance(currentPosition, playerPosition);
        dist /= 100;
        //Debug.Log(dist);
        if (dist < minDist)
        {
            dist = minDist;
        }

        alphaAmount = minDist / dist;

        if (alphaAmount < 0.3f && alphaAmount >= 0f)
        {
            alphaAmount -= 0.2f;
        }

        color.a = alphaAmount;
        
        //color.a = dist;
        sprite.color = color;
	}
}
