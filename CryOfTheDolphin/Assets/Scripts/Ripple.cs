﻿using UnityEngine;
using System.Collections;

public class Ripple : MonoBehaviour {

    public float distanceX, distanceZ;
    public float magnitudeDivider;
    public float[] waveAmplitude;
    public Vector2[] impactPos;
    public float[] distance;
    public float speedWaveSpread;
    private int waveNum;
    private const int maxWaveNum = 5;

    Mesh mesh;


	// Use this for initialization
	void Start () {

        mesh = GetComponent<MeshFilter>().mesh;
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < 4; i++)
        {
            waveAmplitude[i] = renderer.material.GetFloat("_WaveAmplitude" + (i + 1));
            if(waveAmplitude[i] > 0)
            {
                distance[i] += speedWaveSpread;
                renderer.material.SetFloat("_Distance" + (i + 1), distance[i]);
                renderer.material.SetFloat("_WaveAmplitude" + (i + 1), waveAmplitude[i] * 0.98f);
            }
            if(waveAmplitude[i] < 0.01)
            {
                renderer.material.SetFloat("_WaveAmplitude" + (i + 1), 0);
                distance[i] = 0;
            }
        }

	}

    void OnCollisionEnter(Collision col)
    {
        if(col.rigidbody)
        {
            waveNum++;
            if (waveNum == maxWaveNum)
                waveNum = 1;
        }

        waveAmplitude[waveNum - 1] = 0;
        distance[waveNum -1]= 0;
        
        distanceX = this.transform.position.x - col.gameObject.transform.position.x;
        distanceZ = this.transform.position.z - col.gameObject.transform.position.z;
        impactPos[waveNum -1].x = col.transform.position.x;
        impactPos[waveNum-1].y = col.transform.position.z;

        renderer.material.SetFloat("_xImpact" + waveNum, col.transform.position.x);
        renderer.material.SetFloat("_zImpact" + waveNum, col.transform.position.z);


        renderer.material.SetFloat("_OffSetX" + waveNum, distanceX / mesh.bounds.size.x * 2.5f);
        renderer.material.SetFloat("_OffSetZ" + waveNum, distanceZ / mesh.bounds.size.z * 2.5f);

        renderer.material.SetFloat("_WaveAmplitude" + waveNum, col.rigidbody.velocity.magnitude * magnitudeDivider);
    }
}
