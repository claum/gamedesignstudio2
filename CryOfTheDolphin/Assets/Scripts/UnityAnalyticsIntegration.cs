﻿using UnityEngine;
using System.Collections;
using UnityEngine.Cloud.Analytics;

public class UnityAnalyticsIntegration : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
		const string projectId = "f42e1e77-ea13-4eab-8010-adc329915c2f";
		UnityAnalytics.StartSDK (projectId);
		
	}
	
}