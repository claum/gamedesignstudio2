﻿using UnityEngine;
using System.Collections;

public class PauseVolumeControl : MonoBehaviour {

	public Sprite sprite, spriteToggled;
	public int buttonType;

	private SpriteRenderer spriteRenderer;
	private bool mute = false;
	private int width = Screen.width;
	private int height = Screen.height;
	private float sfxSlider, musicSlider;
	private float groupPosX, groupPosY; 
	private float boxWidth, boxHeight;
	private Game game;

	void Start () {
		game = Game.gameInstance;
		sfxSlider = AudioListener.volume;
		musicSlider = game.backgroundLoop.volume;

		boxWidth = width / 4.2666666666f;
		boxHeight = height / 1.6f;
		
		groupPosX = width / 2f - boxWidth / 2f;
		groupPosY = height / 2f - height / 4f;
	}

	void Update () {
		
	}

	void OnGUI() {
		if (game.pause) {
			GUI.BeginGroup (new Rect (groupPosX, groupPosY, 1000, 1000));

			musicSlider = GUI.HorizontalSlider (new Rect (boxWidth / 3f, boxHeight / 3.1f, //2.258f
			                                              boxWidth / 3f, boxHeight / 20f), musicSlider, 0.0f, 0.6f);
			
			game.backgroundLoop.volume = musicSlider;
			game.deathLoop.volume = musicSlider;
			
			sfxSlider = GUI.HorizontalSlider (new Rect (boxWidth / 3f, boxHeight / 2.05f, //1.795f
			                                            boxWidth / 3f, boxHeight / 20f), sfxSlider, 0.0f, 1.0f);
			
			AudioListener.volume = sfxSlider;

			GUI.EndGroup ();
		}
	}


	void OnMouseDown() {
		if (game.pause) {
			switch (buttonType) {
			case 0: buttonMute(); break;
			case 1: break;
			}
		}
	}

	void buttonMute() {
		if (!mute) {
			mute = true;
			AudioListener.volume = 0;
			spriteRenderer.sprite = spriteToggled;
		}
		else {
			mute = false;
			AudioListener.volume = sfxSlider;
			spriteRenderer.sprite = sprite;
		}
	}
	
}
