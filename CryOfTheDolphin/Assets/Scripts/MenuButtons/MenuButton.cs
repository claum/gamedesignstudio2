﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {

	public int buttonType;
	public Sprite sprite;
	public Sprite spriteHover;
    public GameObject bubbleGen;
    BubbleGenerator BG;
    private const int waitTime = 2;

	GameManager GM;

	private SpriteRenderer spriteRenderer;

	void Awake()
	{
		GM = GameManager.instance;
		//GM.OnStateChange += HandleOnStateChange;
		
	}

	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
        BG = bubbleGen.GetComponent<BubbleGenerator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter()
	{
		Debug.Log("Enter");
		spriteRenderer.sprite = spriteHover;
	}

	void OnMouseExit()
	{
		Debug.Log("Leave");
		spriteRenderer.sprite = sprite;
	}

	void OnMouseDown()
	{
		switch (buttonType) 
		{
		case 0: StartCoroutine(buttonStart()); break;
		case 1: StartCoroutine(buttonInstructions()); break;
		case 2: StartCoroutine(buttonCredits()); break;
		case 3: buttonQuit(); break;
		}
	}

    IEnumerator buttonStart()
	{
        BG.NextScene();
		GM = GameManager.instance;
		//start game scene
		GM.SetGameState(GameState.GAME);
		Debug.Log(GM.gameState);
        yield return new WaitForSeconds(waitTime);
		Application.LoadLevel(1);
	}

    IEnumerator buttonInstructions()
	{
        BG.NextScene();
        yield return new WaitForSeconds(waitTime);
		Application.LoadLevel (3);
	}

    IEnumerator buttonCredits()
	{
        BG.NextScene();
        yield return new WaitForSeconds(waitTime);
		Application.LoadLevel(2);
	}

    void buttonQuit()
	{
		Debug.Log("Quit!");
		Application.Quit();
	}

}
