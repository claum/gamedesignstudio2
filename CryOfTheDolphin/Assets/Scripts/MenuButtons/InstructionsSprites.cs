﻿using UnityEngine;
using System.Collections;

public class InstructionsSprites : MonoBehaviour {

	public Sprite[] instructions = new Sprite[4];

	public int arrayPosition;
	private Sprite current;

	void Start () {
		arrayPosition = 0;
		current = instructions [0];
	}

	void Update () {
		current = instructions [arrayPosition];
	}

	public void Increment (){
		if (arrayPosition < 3) {
			arrayPosition += 1;
		}
		updateSprite ();
	}

	public void Decrement () {
		if (arrayPosition > 0) {
			arrayPosition -= 1;
		}
		updateSprite ();
	}

	private void updateSprite()
	{
		gameObject.GetComponent<SpriteRenderer> ().sprite = instructions [arrayPosition];
	}

}
