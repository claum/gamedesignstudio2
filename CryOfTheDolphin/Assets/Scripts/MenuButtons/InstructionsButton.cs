﻿using UnityEngine;
using System.Collections;

public class InstructionsButton : MonoBehaviour {

	public int buttonType;
	public Sprite sprite, spriteHover;
	public GameObject newIS;

	private SpriteRenderer spriteRenderer;
	private InstructionsSprites instructionSprite;

	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();

		//grab the instruction button script reference
		instructionSprite = newIS.GetComponent<InstructionsSprites> ();

		if (buttonType == 2) {
			spriteRenderer.enabled = false;
		}
		instructionSprite.arrayPosition = 0;
	}

	void Update() {
		if (instructionSprite.arrayPosition == 3) {
			if (buttonType == 1) {
				spriteRenderer.enabled = false;
				gameObject.collider2D.enabled = false;
			}
			if (buttonType == 2) {
				spriteRenderer.enabled = true;
				gameObject.collider2D.enabled = true;
			}
		} else {
			if (buttonType == 1) {
				spriteRenderer.enabled = true;
				gameObject.collider2D.enabled = true;
			}
			if (buttonType == 2) {
				spriteRenderer.enabled = false;
				gameObject.collider2D.enabled = false;
			}
		}
	}

	void OnMouseEnter() {
		Debug.Log ("Enter");
		spriteRenderer.sprite = spriteHover;
	}

	void OnMouseExit() {
		Debug.Log ("Leave");
		spriteRenderer.sprite = sprite;
	}

	void OnMouseDown()
	{
		switch (buttonType) {
			case 0:	buttonPrevious (); break;
			case 1:	buttonNext (); break;
			case 2:	buttonStart (); break;
			case 3: buttonMenu (); break;
		}
	}

	void buttonPrevious() {
		instructionSprite.Decrement ();
	}

	void buttonNext() {
		instructionSprite.Increment ();
	}

	void buttonStart() {
		Application.LoadLevel (1);
	}

	void buttonMenu() {
		Application.LoadLevel (0);
	}

}
	