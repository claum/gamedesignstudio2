﻿using UnityEngine;
using System.Collections;

public class CreditButton : MonoBehaviour {
	public int buttonType;
	public Sprite sprite;
	public Sprite spriteHover;
	
	GameManager GM;
	
	private SpriteRenderer spriteRenderer;
	
	void Awake() {
		GM = GameManager.instance;
	}
	
	void Start () {
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	void OnMouseEnter() {
		Debug.Log ("Enter");
		spriteRenderer.sprite = spriteHover;
	}
	
	void OnMouseExit() {
		Debug.Log ("Leave");
		spriteRenderer.sprite = sprite;
	}
	
	void OnMouseDown()
	{
		switch (buttonType) {
		case 0:
			buttonStartGame ();
			break;
		case 1:
			buttonMainMenu ();
			break;
		}
	
	}
	
	void buttonStartGame() {
		Application.LoadLevel (1);
	}
	
	void buttonMainMenu() {
		Application.LoadLevel (0);
	}
}
