﻿using UnityEngine;
using System.Collections;

public class GameOverButtons : MonoBehaviour {

	public int buttonType;
	public Sprite sprite;
	public Sprite spriteHover;

	GameManager GM;
	Game game;

	private SpriteRenderer spriteRenderer;

	void Awake() {
		GM = GameManager.instance;
	}

	void Start () {
		game = Game.gameInstance;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		spriteRenderer.enabled = false;
	}

	void Update () {
		if (game.gameOver) {
			spriteRenderer.enabled = true;
		}
	}

	void OnMouseEnter() {
		if (game.gameOver) {
			Debug.Log ("Enter");
			spriteRenderer.sprite = spriteHover;
		}
	}

	void OnMouseExit() {
		if (game.gameOver) {
			Debug.Log ("Leave");
			spriteRenderer.sprite = sprite;
		}
	}

	void OnMouseDown()
	{
		if (game.gameOver) {
			switch (buttonType) {
			case 0:
				buttonRestart ();
				break;
			case 1:
				buttonMainMenu ();
				break;
			}
		}
	}

	void buttonRestart() {
		Application.LoadLevel (1);
	}

	void buttonMainMenu() {
		Application.LoadLevel (0);
	}
}
