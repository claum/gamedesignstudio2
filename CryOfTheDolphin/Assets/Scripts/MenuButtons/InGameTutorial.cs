﻿using UnityEngine;
using System.Collections;

public class InGameTutorial : MonoBehaviour
{

    Game game;
    SpriteRenderer SR;
    Color color;
    GameObject playerInstance;
    Transform pTransform;
    Animator anim;

    bool tut1, tut2, tut3;
    float waitTime = 4f;
    bool playTut = false;

    // Use this for initialization
    void Start()
    {

        game = Game.gameInstance;
        playerInstance = game.GetPlayer();
        pTransform = playerInstance.GetComponent<Transform>();
        SR = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        color = SR.color;
        color.a = 0;
        SR.color = color;
        tut1 = tut2 = tut3 = false;

    }

    // Update is called once per frame
    void Update()
    {

        checkInput();
        updateMovement();
        if (!playTut)
            RemoveTutorial();

        if (game.level == 1)
        {
            anim.SetBool("Move", true);
            anim.SetBool("Click", false);

            if (!tut1 && playTut)
            {
                DisplayTutorial(1);

                if (Input.GetKey(KeyCode.W))
                {
                    tut1 = true;
                }
            }

        }
        else if (game.level == 4)
        {
            anim.SetBool("Move", false);
            anim.SetBool("Click", true);

            if (!tut2 && playTut)
            {
                DisplayTutorial(2);
                if (Input.GetMouseButton(0))
                {
                    tut2 = true;
                }
            }
        }

    }

    void updateMovement()
    {
        float xPos, yPos;
        xPos = pTransform.position.x + 80;
        yPos = pTransform.position.y + 80;

        transform.position = new Vector3(xPos, yPos, transform.position.z);
    }

    void checkInput()
    {
        float maxWaitTime = 4;

        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
        else
        {
            playTut = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            waitTime = maxWaitTime;
            playTut = false;

        }
        else if (Input.GetMouseButton(0))
        {
            waitTime = maxWaitTime;
            playTut = false;

        }
    }

    void DisplayTutorial(int tut)
    {
        color = SR.color;
        if (color.a < 0)
            color.a = 0;
        color.a += Time.deltaTime;
        SR.color = color;
    }

    void RemoveTutorial()
    {
        color = SR.color;
        if (color.a > 1)
            color.a = 1;
        if (color.a > 0)
        {
            color.a -= Time.deltaTime * 2;
            SR.color = color;
        }
    }
}
