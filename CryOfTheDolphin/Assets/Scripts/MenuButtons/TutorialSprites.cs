﻿using UnityEngine;
using System.Collections;

public class TutorialSprites : MonoBehaviour {

	public Sprite[] tutorials = new Sprite[4];
	public int arrayPosition;

	private Sprite current;
	private int pressCount;
	private bool wasDisplayed = false;
	private bool spawnFinished = false;
	private Game game;
	
	void Start () {
		gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = false;
		game = Game.gameInstance;
		pressCount = 0;
		//Invoke ("ShowInstructions", 5.0f);
		//StartCoroutine ("ShowInstructions");
	}
	
	void Update () {
		current = tutorials [arrayPosition];

		if (Input.GetKey (KeyCode.W)) {
			pressCount++;
		}

		if (!moveCompleted ()) {
			//gameObject.GetComponent<SpriteRenderer>().renderer.enabled = true;
			StartCoroutine("FadeIn");
			StartCoroutine ("ShowMove");
		} 
		else if (moveCompleted()) {
			StopCoroutine("ShowMove");
			//current = null;
			gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = false;
		}

		if (moveCompleted() && !echoCompleted () && game.level == 4) {
			StartCoroutine("FadeIn");
			StartCoroutine ("ShowEcho");
		} else if (echoCompleted()) {
			StopCoroutine("ShowEcho");
			current = null;
			gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = false;
		}

		if (game.level == 5) {
			StartCoroutine("ShowShark");
			//StartCoroutine("Hide");
		} 
		else if (game.level == 5 && spawnFinished) {
			StopCoroutine ("ShowShark");
			current = null;
			gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = false;
		}



	}
	
	private void updateSprite()
	{
		gameObject.GetComponent<SpriteRenderer> ().sprite = tutorials [arrayPosition];
	}

	IEnumerator ShowMove() {
		arrayPosition = 0;
		updateSprite();
		yield return new WaitForSeconds (3f);
		gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = true;
	}
	
	IEnumerator ShowEcho() {
		while (!displayed()) {
			arrayPosition = 1;
			updateSprite();
			yield return new WaitForSeconds(3f);
			gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = true;
		}
	}
	
	IEnumerator ShowShark() {
		arrayPosition = 2;
		updateSprite();
		yield return new WaitForSeconds(0.5f);
		gameObject.GetComponent<SpriteRenderer>().renderer.enabled = true;
		yield return new WaitForSeconds (1f);
		spawnFinished = true;
			//yield return new WaitForSeconds(2.5f);
			//gameObject.GetComponent<SpriteRenderer>().renderer.enabled = false;
		//}
	}

	IEnumerator Hide() {
		yield return new WaitForSeconds(2.5f);
		gameObject.GetComponent<SpriteRenderer>().renderer.enabled = false;
	}

	IEnumerator FadeIn() {
		for (float f = 0f; f <= 1; f += 0.1f) {
			Color c = gameObject.GetComponent<SpriteRenderer>().renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return c;
		}
	}
	
	bool moveCompleted() {
		if (pressCount > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	bool echoCompleted() {
		if (game.level > 3 && moveCompleted() && game.echoCount > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	bool sharkIntroduced() {
		if (game.sharkCount == 1) {
			return true;
		} else {
			return false;
		}
	}

	/*private IEnumerator ShowInstructions() {
		while (!displayed()) {
			yield return new WaitForSeconds (5);
			gameObject.GetComponent<SpriteRenderer> ().renderer.enabled = true;
			arrayPosition = 0;
			current = tutorials [0];
			Time.timeScale = 0f;
		}
	}*/

	public bool displayed() {
		if (gameObject.GetComponent<SpriteRenderer> ().renderer.enabled == true) {
			wasDisplayed = false;
			return true;
		} 
		else {
			wasDisplayed = true;
			return false;
		}
	}
	
}
