﻿using UnityEngine;
using System.Collections;

public class PauseMenuButtons : MonoBehaviour {

	public Sprite sprite, spriteHover;
	public int buttonType;

	private SpriteRenderer spriteRenderer;
    private PauseManager PM;
	Game game;
	GameManager GM;

	void Awake() {
		GM = GameManager.instance;
	}

	void Start () {
		// get components from PauseSliders (???) BUT PERHAPS NOT
		//game = gameObject.GetComponent<Game> ();
		game = Game.gameInstance;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
        PM = GetComponentInParent<PauseManager>();
	}

	void OnMouseEnter() {
		if (game.pause) {
			Debug.Log ("Enter");
			spriteRenderer.sprite = spriteHover;
		}
	}
	
	void OnMouseExit() {
		if (game.pause) {
			Debug.Log ("Leave");
			spriteRenderer.sprite = sprite;
		}
	}

	void OnMouseDown()
	{
		if (game.pause) {
			switch (buttonType) {
				case 0: buttonResume (); break;
				case 1: buttonExitGame (); break;
			}
		}
	}

	void buttonResume() {
        PM.toggleButtons();
        game.pause = false;
		game.unpauseSound.Play();
	}

	void buttonExitGame() {
        PM.toggleButtons();
		game.pause = false;
		Time.timeScale = 1;
		Application.LoadLevel (0);
	}
	
}
