﻿using UnityEngine;
using System.Collections;

public class ScoreParticle : MonoBehaviour {

	private Game game;
	public float goalX;
	public float goalY;
	public float speed;
	private float tick;
	//private Vector3 start;
	private Vector3 goal;
	private float offset;

	//private float startTime;
	//private float journeyLength;
	//private float journeyLengthX;
	void Start() 
	{
		game = Game.gameInstance;
		//start = transform.position;
		goal = new Vector3 (goalX, goalY, transform.position.z);
		//startTime = Time.time;
		//journeyLength = Vector3.Distance(start, goal);
		//journeyLengthX = Mathf.Abs (start.x - goal.x);
		offset = 0;
		//offset = Mathf.Clamp(journeyLength,0,transform.position.y);
	}
	void Update() 
	{
		//float distCovered = (Time.time - startTime) * speed;
		//float fracJourney = tick;//distCovered / journeyLength;
		tick += Time.deltaTime;
		//transform.position = Vector3.Lerp(start, goal, fracJourney);

		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, new Vector3(goal.x,goal.y-offset,goal.z), step);
		//offset = Mathf.Clamp (offset - (Time.deltaTime * 330), 0, 9999);
		transform.Rotate (new Vector3(0,0,10));

		if (Vector3.Distance (transform.position, goal) < 4.0f) 
		{
			game.incrementScore();
			Destroy(gameObject);
		}
		//Debug.Log (Vector3.Distance (start, goal));
	}
}
