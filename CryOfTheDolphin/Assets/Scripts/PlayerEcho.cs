﻿using UnityEngine;
using System.Collections;

public class PlayerEcho : MonoBehaviour {

    public GameObject echo;
	public float echoPerSecond;
    private float echoPerSecondAdd;
	private GameObject echoBar;
	private Game game;
	private float curEcho;
	private float maxEcho;
	private float echoCost;

    public bool hasEcho { get; private set; }
	// Use this for initialization
	void Start () {
		game = Game.gameInstance;
		echoCost = 100;
		maxEcho = 100;
		curEcho = maxEcho;

		//l a z y b o y s
		echoBar = GameObject.FindGameObjectWithTag ("EchoBar");
        echoBar.SetActive(false);

        echoPerSecondAdd = echoPerSecond;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (hasEcho)
        {
            updateEchoRegen();
        }
	}

	void updateEchoRegen()
	{
		curEcho += (echoPerSecond * Time.deltaTime);
		if (curEcho > maxEcho) 
		{
			curEcho = maxEcho;
		}
		updateEchoDisplay ();
	}

	void updateEchoDisplay()
	{
		if (!game.gameOver) 
		{
            if (curEcho >= echoCost)
            {
                echoBar.GetComponent<EchoBar>().updateBar(curEcho, maxEcho, true);
            }
            else
            {
                echoBar.GetComponent<EchoBar>().updateBar(curEcho, maxEcho, false);
            }
		} 
		else 
		{
			echoBar.GetComponent<EchoBar> ().updateBar (0, maxEcho, false);
		}
	}

    void FixedUpdate()
    {
        if (!game.gameOver && Input.GetMouseButtonDown(0) && hasEcho)
        {
			if(curEcho>=echoCost)
			{
            	fireEcho();
				curEcho -= echoCost;
				updateEchoDisplay();
			}
        }
    }

    private void fireEcho()
    {
        float distance = 1000;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, distance))
        {
            //Debug.Log(hit.point);
            GameObject _echo = (GameObject)Instantiate(echo, transform.position, Quaternion.identity);
            _echo.GetComponent<Echo>().setDirection(hit.point - transform.position);
			_echo.GetComponent<Echo>().setSourcePosition(transform.position);
        }
    }

    public void unlockEcho()
    {
        if (!hasEcho)
        {
            //unlock
            hasEcho = true;
            echoBar.SetActive(true);
        }
        else
        {
            //augment
            echoCost = Mathf.Round(echoCost / 2);
            //echoPerSecond += echoPerSecondAdd;
        }
    }

    public Vector3 GetPosition
    {
        get { return transform.position; }
    }

    public void disableEchoBar()
    {
        echoBar.SetActive(false);
    }
}
