﻿using UnityEngine;
using System.Collections;

public class Constants 
{
    public const float XMAX = 665f;
    public const float XMIN = -665f;
    public const float YMAX = 346f;
    public const float YMIN = -346f;

    public const float difficulty = 5f;
    public const float echoUpgrade = 6f;
    public const float startVision = 1.8f;
    public const float visionLoss = 0.1f;
    public const float minVision = 0.4f;
    public const float minWarnDist = 0.6f;

    public const float safeSpawnRadius = 100f;
}
