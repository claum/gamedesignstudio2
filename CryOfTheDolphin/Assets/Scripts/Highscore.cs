﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Highscore : MonoBehaviour {

    private GameManager gameManager;
	// Use this for initialization
	void Start () {
        gameManager = GameManager.instance;
        GetComponent<Text>().text = gameManager.getHighscore().ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
