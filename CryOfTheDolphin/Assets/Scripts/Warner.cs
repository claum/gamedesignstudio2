using UnityEngine;
using System.Collections;

public class Warner : MonoBehaviour
{

    Game game;
    SpriteRenderer sprite;
    float visibleAmount;

	public AudioSource pulse;

    // Use this for initialization
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        game = Game.gameInstance;
        visibleAmount = 0.3f;
    }

    // Update is called once per frame
    void Update()
    {
		pulse.volume = 1.8f*sprite.color.a;

        if (checkAlpha())
        {
            game.ShakeNow = true;
        }

		if (!checkAlpha()) {
			pulse.loop = true;
			pulse.Play();
		}

		if (game.gameOver) {
			pulse.Stop();
		}
    }

    

    bool checkAlpha()
    {
        if (sprite.color.a > visibleAmount)
            return true;
        return false;
    }
}
